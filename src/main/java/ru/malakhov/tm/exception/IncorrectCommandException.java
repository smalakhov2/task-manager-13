package ru.malakhov.tm.exception;

public class IncorrectCommandException extends RuntimeException {

    public IncorrectCommandException() {
        super("Error! Unknow command...");
    }
}
