package ru.malakhov.tm.exception;

public class IncorrectIndexException extends RuntimeException {

    public IncorrectIndexException(final Throwable cause) {
        super(cause);
    }

    public IncorrectIndexException(final String value) {
        super("Error! This value ``" + value + "`` is not number...");
    }

    public IncorrectIndexException() {
        super("Error! Index is incorrect...");
    }

}
