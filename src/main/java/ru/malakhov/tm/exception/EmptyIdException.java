package ru.malakhov.tm.exception;

public class EmptyIdException extends RuntimeException{

    public EmptyIdException() {
        super("Error! Id is empty...");
    }

}
