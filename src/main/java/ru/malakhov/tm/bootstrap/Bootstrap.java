package ru.malakhov.tm.bootstrap;

import ru.malakhov.tm.controller.TaskController;
import ru.malakhov.tm.exception.IncorrectCommandException;
import ru.malakhov.tm.repository.CommandRepository;
import ru.malakhov.tm.repository.ProjectRepository;
import ru.malakhov.tm.repository.TaskRepository;
import ru.malakhov.tm.api.controller.ICommandController;
import ru.malakhov.tm.api.controller.IProjectController;
import ru.malakhov.tm.api.controller.ITaskController;
import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.api.service.ICommandService;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.constant.ArgumentConst;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.controller.CommandController;
import ru.malakhov.tm.controller.ProjectController;
import ru.malakhov.tm.service.CommandService;
import ru.malakhov.tm.service.ProjectService;
import ru.malakhov.tm.service.TaskService;
import ru.malakhov.tm.util.TerminalUtil;


public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository TaskRepository = new TaskRepository();

    private final ITaskService TaskService = new TaskService(TaskRepository);

    private final ITaskController taskController = new TaskController(TaskService);

    private final IProjectRepository ProjectRepository = new ProjectRepository();

    private final IProjectService ProjectService = new ProjectService(ProjectRepository);

    private final IProjectController projectController = new ProjectController(ProjectService);

    private static void displayHello() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    private void process() {
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) throw new IncorrectCommandException();
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.printAbout();
                break;
            case ArgumentConst.HELP:
                commandController.printHelp();
                break;
            case ArgumentConst.VERSION:
                commandController.printVersion();
                break;
            case ArgumentConst.INFO:
                commandController.printInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.printArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.printCommands();
                break;
            default:
                throw new IncorrectCommandException();
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) throw new IncorrectCommandException();
        switch (arg) {
            case TerminalConst.ABOUT:
                commandController.printAbout();
                break;
            case TerminalConst.HELP:
                commandController.printHelp();
                break;
            case TerminalConst.VERSION:
                commandController.printVersion();
                break;
            case TerminalConst.INFO:
                commandController.printInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.printArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.printCommands();
                break;
            case TerminalConst.TASK_LIST:
                taskController.displayTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_DISPLAY_BY_ID:
                taskController.displayTaskById();
                break;
            case TerminalConst.TASK_DISPLAY_BY_NAME:
                taskController.displayTaskByName();
                break;
            case TerminalConst.TASK_DISPLAY_BY_INDEX:
                taskController.displayTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.displayProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_DISPLAY_BY_ID:
                projectController.displayProjectById();
                break;
            case TerminalConst.PROJECT_DISPLAY_BY_NAME:
                projectController.displayProjectByName();
                break;
            case TerminalConst.PROJECT_DISPLAY_BY_INDEX:
                projectController.displayProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
            default:
                parseArg(arg);
                break;
        }
    }

    public void run(final String[] args) {
        displayHello();
        if (parseArgs(args)) commandController.exit();
        process();
    }

}