package ru.malakhov.tm.api.controller;

public interface IProjectController {

    void displayProjects();

    void clearProjects();

    void createProject();

    void displayProjectById();

    void displayProjectByIndex();

    void displayProjectByName();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

}